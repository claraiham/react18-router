// import Navigation from "../../components/Navigation";
import CartItem from "./CartItem";

function Cart() {
  const cart = [
    {
      id: 1,
      title: "Doloribus nesciunt",
      quantity: 2,
    },
    {
      id: 2,
      title: "Doloremque quae",
      quantity: 1,
    },
  ];
  return (
    <>
      {/* <Navigation /> Commenté pour éviter le doublon de la barre de nav */}
      <div className="Cart">
        <h1>Cart</h1>
        <ul>
          {cart.map((item) => (
            <CartItem key={item.id} {...item} />
          ))}
        </ul>
      </div>
    </>
  );
}

export default Cart;
