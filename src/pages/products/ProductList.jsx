// import Navigation from "../../components/Navigation";
import ProductItem from "./ProductItem";

function ProductList() {
  const products = [
    {
      id: 1,
      title: "Exercitationem accusantium",
      description: "Fuga consequatur, odio inventore",
    },
    {
      id: 2,
      title: "Labore inventore",
      description: "Esse delectus eligendi adipisci",
    },
    {
      id: 3,
      title: "Exercitationem accusantium",
      description: "Aut veritatis aliquid quam esse",
    },
  ];
  return (
    <>
      {/* <Navigation /> Commenté pour éviter le doublon de la barre de nav*/} 
      <div className="ProductList">
        <h1>Products</h1>
        <ul>
          {products.map((product) => (
            <ProductItem key={product.id} {...product} />
          ))}
        </ul>
      </div>
    </>
  );
}

export default ProductList;
