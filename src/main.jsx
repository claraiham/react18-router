import React from "react";
import ReactDOM from "react-dom/client";
import Cart from "./pages/cart/Cart";
import Home from "./pages/home/Home";
import ProductList from "./pages/products/ProductList";

// TODO install and import react router

import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom"; // import de react-router

import "./index.css";
import Navigation from "./components/Navigation";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Navigation/>,
    children:[ // enfants de ma barre de navigation
      {
        path:"Home",// nom du chemin défini par "Home"
        element:<Home/>, // composant qui est rattaché au path(ici ma vue "home")
      },
      {
        path:"Products",//nom du chemin
        element:<ProductList/>, // composant qui est rattaché au path
      },
      {
        path:"Cart",// nom du chemin
        element:<Cart/>, // composant qui est rattaché au path
      },
    ]
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    {/* TODO use react router here */}

    <RouterProvider router={router} />

  </React.StrictMode>
);
