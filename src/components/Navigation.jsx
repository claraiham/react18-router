import "./Navigation.css";

import { Link, Outlet } from "react-router-dom"; // Y'en a besoin pour que ça marche


//link avec un grand L

function Navigation() {
  return (
    <div>
      <div>
        <ul className="Navigation">
          {/* TODO add links to the different pages */}

          <li>
            <Link to={`Home`}>Home</Link> {/* Récupère le lien cintenu dans "child" de "router" */}
          </li>
          <li>
            <Link to={`Products`}>Products</Link>{/* Récupère le lien cintenu dans "child" de "router" */}
          </li>
          <li>
            <Link to={`Cart`}>Cart</Link>{/* Récupère le lien cintenu dans "child" de "router" */}
          </li>
        </ul>
      </div>
      <div className="detail">
        <Outlet /> {/* Affiche le contenu des pages par magie */}
      </div>
    </div>

  );
}

export default Navigation;
